﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Автосервис
{
    public partial class просмотрТоваров : Form
    {
        public просмотрТоваров()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Просмотр_товаров_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "автосервисDataSet.Manufacturer". При необходимости она может быть перемещена или удалена.
            this.manufacturerTableAdapter.Fill(this.автосервисDataSet.Manufacturer);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "автосервисDataSet.Product". При необходимости она может быть перемещена или удалена.
            this.productTableAdapter.Fill(this.автосервисDataSet.Product);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            каталогТоваров каталогТоваров = new каталогТоваров();
            каталогТоваров.Show();
            Hide();
                
        }
    }
}
